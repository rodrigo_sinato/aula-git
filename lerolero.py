"""gerador de lerolero

gera frases de feito sem significado real"""

import random

# cada frase e composta por tres partes aleatorias; aqui,
# listas de possibilidades para cada uma das partes

parte1 = [
    "o sistema em desenvolvimento",
    "o novo protocolo de comunicacao",
    "o algoritmo foi otimizado e"
]
parte2 = [
    "possui excelente desempenho",
    "oferece garantias de precisao",
    "preenche uma lacuna"
]
parte3 = [
    "nas aplicacoes a que se destina",
    "em relacao as demais opcoes",
    "provendo ampla blablabla"
]

lingua = int("Escolha a língua: 1 - português; 2 - inglês \n ")

if lingua == 2:
    parte1 = []
    parte2 = []
    parte3 = []

# combina as partes aleatoriamente
print(random.choice(parte1), random.choice(parte2), random.choice(parte3))